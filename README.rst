=======
testapi
=======


Project to stress test UCless api endpoints getAvailable and getPayload.


Description
===========

  The program will stress test UCless api endpoints getAvailable and getPayload in the following way.

  The project consists of a series of small command-line utilities to probe the endpoints. Currently
  there exists: **available, payload, plotavailable**. All utilities support multiple command-line
  options with sensible defaults to facilitate convenient use.

  1. **available**

      This utility collects information on available data sets for a range of dates specified by a start 
      and end date.

      The getAvailable endpoint is systematically probed for all data on every date in the specified range.
      There are two report options. In the default mode, the dates are retained with a count of the 
      number of data files available for each date. In the other mode, a complete list of filenames is
      retrieved from the file system. The structured JSON output can be sent to stdout or to a file.
      If the count list is stored, this can be read and processed by the plotavailable utility.

      Options are available to specifiy serial number, server URL, output files, and log levels among other things.

  2. **plotavailable**

      This utility reads the file created by the **availble** utility and generates a graphical calendar representation 
      of the data. 

  3. **payload**

      This utilty retrieves the data files from the server for all dates in a specified date range. The options
      are similar to those for **available**, but there is also an option to specify an output file. 
      Default values are available for testing.

.. _pyscaffold-notes:

Note
====

This project has been set up using PyScaffold 4.0. For details and usage
information on PyScaffold see https://pyscaffold.org/.
