"""
This is a small app to collect data from the UCless endpoint getAvailable.

To run this script, type "python getavailable.py -h" for usage info.

The script generates a series of endpoint queries based on default or supplied parameters and returns the results.

e.g.
payload = { "authKey": "7e4ece53-a3a6-470e-a01c-ccb3ce1c8428", "serialNumber": "XTRA200829001", "searchDateUTC": "2021-02-19T15:47:08Z" }

It collects the information and writes out a json file with the serial number, year, and list of dates with files including the number of files/day.
"""


import requests
import json
import argparse
import logging
import sys

from datetime import datetime, timedelta
from testapi import __version__

__author__ = "Bob Tatar"
__copyright__ = "Bob Tatar"
__license__ = "Proprietary"

_logger = logging.getLogger(__name__)

#URL = 'http://10.10.10.112:8585'
#serialNumber = "XTRA200829001"
#year = 2020
#month = 1
#day = 1
#availableFileName='available.json'
#authKey = '7e4ece53-a3a6-470e-a01c-ccb3ce1c8428'
#firstDate = '2021-12-01'
#lastDate = '2021-12-31'

def getAvailable(serialNumber, year, month, day, server_URL, authKey):

    _logger.debug(serialNumber)
    _logger.debug(year)
    _logger.debug(authKey)
    _logger.debug(server_URL)

    Full_URL = server_URL + '/api/getAvailable'
    _logger.debug(Full_URL)

    mydate = datetime(year, month, day)
    sdate = mydate.strftime('%Y-%m-%dT%H:%M:%SZ')

    body = { "authKey": authKey, "serialNumber": serialNumber, "searchDateUTC": sdate }
    r = requests.post(Full_URL, json=body)
    d = json.loads(r.text)

    _logger.debug(d)

    return(d)


def getAvailableRange(serialNumber, firstDate, lastDate, server_URL, authKey):

    _logger.debug(serialNumber)
    _logger.debug(authKey)

    mydate = datetime.strptime(firstDate, '%Y-%m-%d')
    _logger.debug(mydate)

    enddate = datetime.strptime(lastDate, '%Y-%m-%d')
    _logger.debug(enddate)

#    tmp = datetime(year, 12, 31)
#    enddate = datetime(tmp.year, tmp.month, tmp.day)
#    mydate = datetime(year, 1, 1)
#    mydict = dict()

    lendict = dict()
    lendict['searchDateTime'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    lendict['firstDate'] = mydate.strftime('%Y-%m-%d')
    lendict['lastDate'] = enddate.strftime('%Y-%m-%d')

    lendict['serialNumber'] = serialNumber

    mykey = mydate.strftime('%Y-%m-%d')
    _logger.debug(mykey)

    while mydate <= enddate:
        d = getAvailable(serialNumber, mydate.year, mydate.month, mydate.day, server_URL, authKey)

#        if d['status'] == 'valid' and len(d[serialNumber]) > 1 :

        _logger.debug(f" payload length = {len(d['payload'])}")
        if d['status'] == 'valid' and len(d['payload']) > 0 :
#            mydict[mykey] = d[serialNumber]
#            lendict[mykey] = len(d[serialNumber])
            nfiles = 0
            for item in d['payload']:
                _logger.debug(f' item = {item}')
                nfiles += len(item['data'])
                _logger.debug(f' nfiles = {nfiles}')

            _logger.debug(nfiles)
            if nfiles > 0:
                lendict[mykey] = nfiles

#            lendict[mykey] = len(d['payload'])

        mydate += timedelta(days=1)
        sdate = mydate.strftime('%Y-%m-%dT%H:%M:%SZ')
        mykey = mydate.strftime('%Y-%m-%d')

    _logger.debug(lendict)

    return(lendict)

def writeAvailable(lendict, availableFileName):
    with open(availableFileName, 'wb', 0) as availableFile:
        _logger.info(f' Writing to {availableFileName}')
        availableFile.write( (json.dumps(lendict)).encode('utf-8') )
        _logger.info(' Done.')

    return

def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Check available data on datapit.")

    parser.add_argument('-s', '--serialNumber', dest='serialNumber', help='Serial number of analyzer to search.', default='XTRA200829001')
    parser.add_argument('-f', '--firstDate', dest='firstDate', help='First date to search datapit in YYYY-MM-DD format.', default='2021-01-01')
    parser.add_argument('-l', '--lastDate', dest='lastDate', help='Last date to search datapit in YYYY-MM-DD format.', default='2021-12-31')
    parser.add_argument('-a', '--authKey', dest='authKey', help='Authorization key if not available locally in config.json.', default='7e4ece53-a3a6-470e-a01c-ccb3ce1c8428')
    parser.add_argument('-u', '--URL', dest='URL', help='Server server_URL for tests.', default='http://10.10.10.112:8585')
    parser.add_argument('-o', '--output', dest='availableFileName', help='Name of output file to store the results.', default='available.json')
    parser.add_argument('--version', action='version', version='testapi {ver}'.format(ver=__version__))

    parser.add_argument('-v', '--verbose', dest='loglevel', help='set loglevel to INFO', action="store_const", const=logging.INFO)
    parser.add_argument('-vv','--very-verbose', dest='loglevel', help='set loglevel to DEBUG', action='store_const', const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`getavailable` to be called with string arguments in a CLI fashion

    Instead of returning the value from :func:`getavailable`, it writes the results to
    availableFile and prints the result to ``stdout``.

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--verbose", "2020"]``).
    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info(" Starting...")
    avail = getAvailableRange(args.serialNumber, args.firstDate, args.lastDate, args.URL, args.authKey)

    writeAvailable(avail, args.availableFileName)
    _logger.info(json.dumps(avail, indent=2))
    _logger.info(" Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m testapi.testapi
    #
    run()
