"""
This module defines a class to explore the UCless endpoint getAvailable.

To run this script, type "python available.py -h" for usage info.

The script can generate endpoint queries based on default or supplied parameters and return result.
e.g.
payload = { "authKey": "7e4ece53-a3a6-470e-a01c-ccb3ce1c8428", "serialNumber": "XTRA200829001", "searchDateUTC": "2021-02-19T15:47:08Z" }

It collects the information and prints out a json string with the serial number, year, and list of dates with files including the number of files/day.

"""

import requests
import json
import argparse
import logging
import sys

from datetime import datetime, timedelta
from testapi import __version__

__author__ = "Bob Tatar"
__copyright__ = "Bob Tatar"
__license__ = "Proprietary"

_logger = logging.getLogger(__name__)


class Available:
    def __init__(self, server_URL, authKey):
        self.server_URL = server_URL
        self.authKey = authKey
        self.Full_URL = self.server_URL + '/api/getAvailable'
        self.serialNumber = ''
        self.lendict = dict()
        self.filedict = dict()

        _logger.debug(self.serialNumber)
        _logger.debug(self.server_URL)
        _logger.debug(self.Full_URL)


    def getAvailable(self, searchDate):
        """ takes datetime arguments"""

        _mydate = searchDate
        _sdate = _mydate.strftime('%Y-%m-%dT%H:%M:%SZ')
        _logger.info(f' Querying {_sdate}')

        _body = { "authKey": self.authKey, "serialNumber": self.serialNumber, "searchDateUTC": _sdate }
        r = requests.post(self.Full_URL, json=_body)
        d = json.loads(r.text)
        _logger.debug(d)
        return(d)


    def getAvailableRangeLengths(self, firstDate, lastDate):
        """ takes string arguments"""

        _mydate = datetime.strptime(firstDate, '%Y-%m-%d')
        _logger.debug(_mydate)

        _enddate = datetime.strptime(lastDate, '%Y-%m-%d')
        _logger.debug(_enddate)

        self.lendict['serialNumber'] = self.serialNumber

        _mykey = _mydate.strftime('%Y-%m-%d')
        _logger.debug(_mykey)

        while _mydate <= _enddate:
            _d = self.getAvailable(_mydate)

            _logger.debug(f" payload length = {len(_d['payload'])}")
            if _d['status'] == 'valid' and len(_d['payload']) > 0 :
                _nfiles = 0
                for _item in _d['payload']:
                    _logger.debug(f' item = {_item}')
                    _nfiles += len(_item['data'])
                    _logger.debug(f' nfiles = {_nfiles}')

                _logger.debug(_nfiles)
                if _nfiles > 0:
                    self.lendict[_mykey] = _nfiles

            _mydate += timedelta(days=1)
            _sdate = _mydate.strftime('%Y-%m-%dT%H:%M:%SZ')
            _mykey = _mydate.strftime('%Y-%m-%d')

        _logger.debug(self.lendict)

        return(self.lendict)

    def getAvailableRangeFiles(self, firstDate, lastDate):
        """ takes string arguments"""

        _mydate = datetime.strptime(firstDate, '%Y-%m-%d')
        _logger.debug(_mydate)

        _enddate = datetime.strptime(lastDate, '%Y-%m-%d')
        _logger.debug(_enddate)

        self.filedict['serialNumber'] = self.serialNumber

        _mykey = _mydate.strftime('%Y-%m-%d')
        _logger.debug(_mykey)

        _files = []
        while _mydate <= _enddate:
            _d = self.getAvailable(_mydate)

            if _d['status'] == 'valid' and len(_d['payload']) > 0 :
                _logger.debug(f" payload = {_d['payload']}")
                _logger.debug(f" payload length = {len(_d['payload'])}")
                for _item in _d['payload']:
                    _logger.debug(f' item = {_item}')
                    aa = _item['data']
                    if len(aa) > 0:
                      _files += aa

            _mydate += timedelta(days=1)
            _sdate = _mydate.strftime('%Y-%m-%dT%H:%M:%SZ')
            _mykey = _mydate.strftime('%Y-%m-%d')

        _logger.debug(_files)
        self.filedict['files'] = _files

        return(self.filedict)

    def writeAvailable(self, _availableFileName):
        with open(_availableFileName, 'wb', 0) as _availableFile:
            _logger.info(f' Writing to {_availableFileName}')
            _availableFile.write( (json.dumps(self.filedict, indent=2)).encode('utf-8') )
            _logger.info(' Done.')

        return


def writeReportDict(reportDict, reportFileName):
    with open(reportFileName, 'wb', 0) as reportFile:
        _logger.info(f' Writing to {reportFileName}')
        reportFile.write( (json.dumps(reportDict, indent=2)).encode('utf-8') )
        _logger.info(' Done.')
    return

def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Check available data on datapit.")

    parser.add_argument('-s', '--serialNumbers', nargs='*', type=str, help='One or more serial numbers of analyzers to search. default = XTRA200829001', default=['XTRA200829001'])
    parser.add_argument('-f', '--firstDate', help='First date to search datapit in YYYY-MM-DD format. default = 2021-01-01', default='2021-01-01')
    parser.add_argument('-l', '--lastDate', help='Last date to search datapit in YYYY-MM-DD format. default = 2021-12-31', default='2021-12-31')
    parser.add_argument('-a', '--authKey', help='Authorization key. default = <eric delich key>', default='7e4ece53-a3a6-470e-a01c-ccb3ce1c8428')
    parser.add_argument('-u', '--URL', dest='URL', help='Server server_URL for tests. default = https://datapit.xos.com', default='https://datapit.xos.com')
    parser.add_argument('--examples', action='store_true', help='Show usage examples.', default=False)

    parser.add_argument('-o', '--output', dest='reportFileName', help='Name of output file to store the results. default = <STDOUT>')
    parser.add_argument('-C', '--countAvailable', action='store_true', help='Count the available files. default = False', default=False)
    parser.add_argument('--version', action='version', version='testapi {ver}'.format(ver=__version__))

    parser.add_argument('-v', '--verbose', dest='loglevel', help='set loglevel to INFO', action="store_const", const=logging.INFO)
    parser.add_argument('-vv','--very-verbose', dest='loglevel', help='set loglevel to DEBUG', action='store_const', const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`available` to be called with optional string arguments in a CLI fashion

    Instead of returning the value from :func:`available`, it writes the results to
    reportFile or prints the result to ``stdout``.

    # Get list of files vailability for one month and view on console using default key and serial #
    python available.py -f 2020-01-01 -l 2020-01-31

    # Get daily count of data for one year and save results to 2020.json with
    python available.py -f 2020-01-01 -l 2020-12-31 -C -o 2020Jan.json

    # Get daily count of data for one year on test url
    python available.py -f 2020-01-01 -l 2020-12-31 -C -u http://10.10.10.112:8585

    # Get list of files for one year and save results to 2020.json using special authorization key and SN
    python available.py -f 2020-01-01 -l 2020-12-31 -o 2020Jan.json -s XTRA200829001 -a 7e4ece53-a3a6-470e-a01c-ccb3ce1c8428

    """
    args = parse_args(args)

    if args.examples:
        print(main.__doc__)
        sys.exit(0)

    setup_logging(args.loglevel)
    _logger.info(" Starting...")
    avail = Available(args.URL, args.authKey)
    rdict = dict()
    rdict['searchDateTime'] = datetime.now().strftime('%Y-%m-%dT%H:%M:%SZ')
    rdict['firstDate'] = args.firstDate + 'T:00:00:00Z'
    rdict['lastDate'] = args.lastDate + 'T:23:59:59Z'

    for sn in args.serialNumbers:

        avail.serialNumber = sn
        _logger.info(' serialNumber = ' + sn)
        if args.countAvailable:
            report = avail.getAvailableRangeLengths(args.firstDate, args.lastDate)
            rdict['countlist'] = report
        else:
            report = avail.getAvailableRangeFiles(args.firstDate, args.lastDate)
            rdict['filelist'] = report

    if args.reportFileName is None:
      print(json.dumps(rdict, indent=2))
    else:
      writeReportDict(rdict, args.reportFileName)

    _logger.info(" Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m testapi.testapi
    #
    run()
