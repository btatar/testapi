"""
This module defines a class to explore the UCless endpoint getPayload.

To run this script, type "python payload.py -h" for usage info.

The script generates enpoint queries based on default or supplied parameters and returns results.

e.g.
body = { "authKey": "7e4ece53-a3a6-470e-a01c-ccb3ce1c8428", "serialNumber": "XTRA200829001", "searchDateUTC": "2021-02-19T15:47:08Z" }

"""

from available import Available
from readdb import SerialNumberDB
import requests
import json
import argparse
import logging
import sys
import os

from datetime import datetime, timedelta
from testapi import __version__

__author__ = "Bob Tatar"
__copyright__ = "Bob Tatar"
__license__ = "Proprietary"

_logger = logging.getLogger(__name__)

class Payload:
    def __init__(self, server_URL, authKey):
        self.server_URL = server_URL
        self.Payload_URL = server_URL + '/api/getPayload'
        self.available = Available( self.authKey, self.server_URL)
        self.fileOut = False
        self.authKey = authKey
        self.serialNumber = ''
        self.payload = ''
        self.directory = ''

        _logger.debug(' init: ' + self.authKey)
        _logger.debug(' init: ' + self.server_URL)
        _logger.debug(self.Payload_URL)


    def getPayload(self, payloadFileName):
        body = { "authKey": self.authKey, "target": payloadFileName }
        r = requests.post(self.Payload_URL, json=body)
        self.payload = r.text

        if self.fileOut:
           _where = self.directory
        else:
           _where = 'STDOUT'

        _logger.info(' Retrieving: ' + payloadFileName + ' to: ' + _where)
        _logger.debug(self.payload)

        if self.fileOut:
            self.writePayload(payloadFileName)
        else:
            print(self.payload)

        return r.status_code


    def writePayload(self, payloadFileName):
        outputFileName = self.directory + payloadFileName
        with open(outputFileName, 'wb', 0) as payloadFile:
           payloadFile.write( self.payload.encode('utf-8') )
        return


    def getPayloadDay(self, payloadDate):
        """ takes string arguments"""

        myDate = datetime.strptime(payloadDate, '%Y-%m-%d')

        _logger.debug(f' available = {self.available}')

        avail = self.available.getAvailable(myDate)
        item = avail['payload']
        _logger.debug(f' item = {item}')

        filenames = item[0]['data']
        _logger.debug(f' filenames = {filenames}')

        for filename in filenames:
            result = self.getPayload(filename)
        return


    def getPayloadRange(self, firstDate, lastDate):
        """ takes string arguments"""

        _logger.info(' Serial Number = ' + self.serialNumber)
        mydate = datetime.strptime(firstDate, '%Y-%m-%d')
        enddate = datetime.strptime(lastDate, '%Y-%m-%d')

        filedict = self.available.getAvailableRangeFiles(firstDate, lastDate)
        for file in filedict['files']:
            _logger.debug(f' file = {file}')
            self.getPayload(file)
        return


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Check available data on datapit.")
    parser.add_argument('-s', '--serialNumbers', nargs='*', type=str, help='One or more serial numbers of analyzers to search. default = XTRA200829001', default=['XTRA200829001'])
    parser.add_argument('-f', '--firstDate', help='First date to search datapit in YYYY-MM-DD format. default = 2021-01-01', default='2021-01-01')
    parser.add_argument('-l', '--lastDate', help='Last date to search datapit in YYYY-MM-DD format. default = 2021-12-31', default='2021-12-31')
    parser.add_argument('-a', '--authKey', help='Authorization key. default = <eric delich key>', default='7e4ece53-a3a6-470e-a01c-ccb3ce1c8428')
    parser.add_argument('-u', '--URL', dest='URL', help='Server server_URL for tests. default = https://datapit.xos.com', default='https://datapit.xos.com')
    parser.add_argument('--examples', action='store_true', help='Show usage examples.', default=False)

    parser.add_argument('-d', '--directory', dest='directory', help='Name of output directory path to store the results.')
    parser.add_argument('--version', action='version', version='testapi {ver}'.format(ver=__version__))

    parser.add_argument('-v', '--verbose', dest='loglevel', help='set loglevel to INFO', action="store_const", const=logging.INFO)
    parser.add_argument('-vv','--very-verbose', dest='loglevel', help='set loglevel to DEBUG', action='store_const', const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`payload` to be called with optional string arguments in a CLI fashion

    Instead of returning the value from :func:`payload`, it writes the results to
    reportFile or prints the result to ``stdout``.

    # Get content of all JSON files availabile for one day and view on console using default key and serial #
    python payload.py -f 2020-12-14 -l 2020-12-14

    # Get content of all JSON files for 2020 and save results to directory 2020_data
    python payload.py -f 2020-01-01 -l 2020-12-31 -d ./2020_data/

    # Get content of  of JSON files for 2020 from test url
    python payload.py -f 2020-01-01 -l 2020-12-31 -u http://10.10.10.112:8585

    # Get content of all JSON files one year and save results to directory 2021Jan using special key and SN
    python payload.py -f 2020-01-01 -l 2020-12-31 -d ./2020Jan/ -s XTRA200829001 -s 7e4ece53-a3a6-470e-a01c-ccb3ce1c8428

    """

    args = parse_args(args)

    if args.examples:
        print(main.__doc__)
        sys.exit(0)

    setup_logging(args.loglevel)
    _logger.info(" Starting...")

    date = args.firstDate.split('-')

    _logger.debug(args)

    payload = Payload( args.URL)

    if args.directory is None:
       payload.fileOut = False
    else:
       payload.fileOut = True
       if not os.path.exists(args.directory):
          os.makedirs(args.directory)
       payload.directory = args.directory

    for sn in args.serialNumbers:

      payload.serialNumber = sn
      ff = payload.getPayloadRange(args.firstDate, args.lastDate)

    _logger.info(" Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m testapi.testapi
    #
    run()
