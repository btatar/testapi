"""
This is a small app to plot the getavailable.json from the getavailable utility.

To run this script, type "plotavaialble.py" with or without arguments.

Type "plotavailable -h" to see more usage information.

The script generates a calendar plot of the data with the number of files available for a given day.

# Make sure system has numpy and matplotlib
#   pip install numpy
#   pip install matplotlib
#
# To fix numpy issue on base image:
#   sudo apt-get install python-dev libatlas-base-dev
"""

import matplotlib.transforms
from datetime import datetime
import calendar
import numpy as np
from matplotlib.patches import Rectangle
import matplotlib.pyplot as plt
import json
import sys
import argparse
import logging

from testapi import __version__

__author__ = "Bob Tatar"
__copyright__ = "Bob Tatar"
__license__ = "Proprietary"

_logger = logging.getLogger(__name__)

def leap(year):
    if (year % 4) == 0:
        if (year % 100) == 0:
            if (year % 400) == 0:
               return True
            else:
               return False
        else:
            return True
    else:
        return False

def plot_calendar(days, months, year, available, title):
    plt.figure(figsize=(11, 5))
    # non days are grayed
    ax = plt.gca().axes
    size = 1.0
    alpha = 1.0
    missing = 'black'

    if not leap(year):
        ax.add_patch(Rectangle((29, 2), width=size, height=size, color=missing, alpha=alpha))

    ax.add_patch(Rectangle((30, 2), width=size, height=size, color=missing, alpha=alpha))
    ax.add_patch(Rectangle((31, 2), width=size, height=size, color=missing, alpha=alpha))
    ax.add_patch(Rectangle((31, 4), width=size, height=size, color=missing, alpha=alpha))
    ax.add_patch(Rectangle((31, 6), width=size, height=size, color=missing, alpha=alpha))
    ax.add_patch(Rectangle((31, 9), width=size, height=size, color=missing, alpha=alpha))
    ax.add_patch(Rectangle((31, 11), width=size, height=size, color=missing, alpha=alpha))
    for d, m, a in zip(days, months, available):
        ax.add_patch(Rectangle((d, m), width=size, height=size, color='C0'))
        ax.text(d, m, a, ha="left", va="top", color="black")

    plt.yticks(np.arange(1, 13), list(calendar.month_abbr)[1:])
    plt.xticks(np.arange(1,32), np.arange(1,32))
    plt.xlim(1, 32)
    plt.ylim(1, 13)
    plt.gca().invert_yaxis()
    # remove borders and ticks
    for spine in plt.gca().spines.values():
        spine.set_visible(True)
#
    plt.xticks(ha='left')
    plt.yticks(va='top')
    plt.tick_params(top=False, bottom=False, left=False, right=False)
    plt.grid(True)
    plt.suptitle(title)
    return plt


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Check available data on datapit.")
    parser.add_argument('-t', '--title', dest='title', help='String to use for plot title instead of default string.')
    parser.add_argument('-o', '--output', dest='outputFileName', help='Name of output file to store image.', default='DISPLAY')
    parser.add_argument('-i', '--input', dest='inputFileName', help='Name of input file containing available count data.', default='available.json')
    parser.add_argument('--version', action='version', version='testapi {ver}'.format(ver=__version__))
    parser.add_argument('-v', '--verbose', dest='loglevel', help='set loglevel to INFO', action="store_const", const=logging.INFO)
    parser.add_argument('-vv','--very-verbose', dest='loglevel', help='set loglevel to DEBUG', action='store_const', const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """Wrapper allowing :func:`getavailable` to be called with string arguments in a CLI fashion

    Instead of returning the value from :func:`getavailable`, it writes the results to
    availableFile and prints the result to ``stdout``.

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--verbose", "2020"]``).
    """

    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info(" Starting...")

    availableFileName = args.inputFileName
    _logger.info(f' input = {availableFileName}')
    with open(availableFileName, "rb", 0) as availableFile:
        caldata = json.loads(availableFile.read().decode('utf-8'))

    _logger.debug(caldata)

    firstDate = caldata['firstDate']
    _logger.info(f' firstDate = {firstDate}')

    year = int(firstDate.split('-')[0])
    del caldata['firstDate']

    lastDate = caldata['lastDate']
    _logger.info(f' lastDate = {lastDate}')
    del caldata['lastDate']

    searchDateTime = caldata['searchDateTime']
    _logger.info(f' searchDateTime = {searchDateTime}')
    del caldata['searchDateTime']

    sn = list(caldata.keys())
    data = caldata[sn[0]]

    serialNumber = data['serialNumber']
    _logger.info(f' serialNumber = {serialNumber}')
    del data['serialNumber']

    dates = list(data.keys())
    available = list(data.values())

    days = [ int(x.split('-')[2]) for x in dates ]
    months = [ int(x.split('-')[1]) for x in dates ]

    if args.title is None:
      title = f'Dates (blue) from {firstDate} to {lastDate} for analyzer {serialNumber} showing available data.'
    else:
      title = args.title

    myplot = plot_calendar(days, months, year, available, title)

    if args.outputFileName == 'DISPLAY':
        myplot.show()
    else:
        myplot.savefig(args.outputFileName)

    _logger.info(" Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m testapi.getavailable
    #
    run()
