"""
This is a small app to read a small database of serial numbers and authorization keys referenced by serial numbers.

This is intended as a utility for the testapi scripts.
"""

import json
import argparse
import logging
import sys

from testapi import __version__

__author__ = "Bob Tatar"
__copyright__ = "Bob Tatar"
__license__ = "Proprietary"

_logger = logging.getLogger(__name__)

class SerialNumberDB:
    def __init__(self, dbFilename):

        self.dbFilename = dbFilename

        _sndb = self.readDB(self.dbFilename)
        _logger.debug(_sndb)

# create list of serials
        _serials = _sndb['serials']
        _logger.debug(f' In class: {json.dumps(_serials, indent=2)}')
# create list of auth keys
        _keys = _sndb['keys']
        _logger.debug(f' In classs: {json.dumps(_keys, indent=2)}')
        self.db = {}

# Convert the json file into a more convenient lookup table
        for _unit in _serials:
            _name = _unit['name']
            _sn = _unit['serialNumber']
            for _item in _keys:
                _item_name = _item['assigned']
                if _name == _item_name:
                    self.db[_sn] = _item['uuid']

        _logger.debug(json.dumps(self.db, indent=2))


    def readDB(self, dbFileName):
        try:
            with open(dbFileName, 'rb', 0) as dbFile:
                _logger.debug(f' Reading from {dbFileName}')
                db = json.loads(dbFile.read().decode('utf-8'))
                _logger.debug(json.dumps(db, indent=2))
                _logger.debug(f' Done.')
        except IOError as e:
            print(e)
        except:
            print('Exception in file IO.')
            db = dict()

        return db

    def serials(self):
        return list(self.db.keys())

    def getKey(self, serialNumber):
        _logger.debug(self.db)
        _key = self.db[serialNumber]
        _logger.info(_key)
        return _key


def parse_args(args):
    """Parse command line parameters

    Args:
      args (List[str]): command line parameters as list of strings
          (for example  ``["--help"]``).

    Returns:
      :obj:`argparse.Namespace`: command line parameters namespace
    """
    parser = argparse.ArgumentParser(description="Check available data on datapit.")

    parser.add_argument('-s', '--serialNumbers', nargs="*", help='One or more serial numbers of analyzers to search.')
    parser.add_argument('-i', '--input', dest='dbFileName', help='File name of serial number database.', default='sndb.json')
    parser.add_argument('-S', '--showAvailableSN', action='store_true', help='Display avaliable serial numbers.', default=False)
    parser.add_argument('-K', '--showAuthKey', action='store_true', help='Display Authorization key(s) for serial number(s).', default=False)
    parser.add_argument('--version', action='version', version='testapi {ver}'.format(ver=__version__))

    parser.add_argument('--examples', action='store_true', help='Display usage examples.')
    parser.add_argument('-v', '--verbose', dest='loglevel', help='set loglevel to INFO', action="store_const", const=logging.INFO)
    parser.add_argument('-vv','--very-verbose', dest='loglevel', help='set loglevel to DEBUG', action='store_const', const=logging.DEBUG)
    return parser.parse_args(args)


def setup_logging(loglevel):
    """Setup basic logging

    Args:
      loglevel (int): minimum loglevel for emitting messages
    """
    logformat = "[%(asctime)s] %(levelname)s:%(name)s:%(message)s"
    logging.basicConfig(
        level=loglevel, stream=sys.stdout, format=logformat, datefmt="%Y-%m-%d %H:%M:%S"
    )


def main(args):
    """
    Wrapper allowing methods from the class SerialNumberDB to be called with string arguments.
    All results are returned to ``stdout``.

    Typical usage examples:

    1. Get a list of serial numbers in the file sndb.json.

    python `main` -S

    2. Get a list of serial numbers in the file myfile.json.

    python readdb.py -S -i myfile.json

    3. Get a list of authentication keys from file myfile.json.

    python readdb.py -K -i myfile.json

    4. Get the authetnication key for a particular serial number in the default sndb.json file.

    python readdb.py -s XTRA190610001 -K

    """
    args = parse_args(args)
    setup_logging(args.loglevel)
    _logger.info(" Starting...")
    _logger.info(args.serialNumbers)
    sndb = SerialNumberDB(args.dbFileName)

#    print(sndb.serials())

    _logger.debug(args)
    if args.showAvailableSN:
      for sn in sndb.serials():
        print(sn)

    if args.serialNumbers is None:
      serialNumbers = sndb.serials()
    else:
      serialNumbers = args.serialNumbers

    if args.showAuthKey:
      for sn in serialNumbers:
        print(sndb.getKey(sn))

    if args.examples:
      print(main.__doc__)

#    print(json.dumps(sndb.serials, indent=2))
#    writeAvailable(sndb, args.dbFileName)
#    _logger.info(json.dumps(sndb, indent=2))
    _logger.info(" Finished.")


def run():
    """Calls :func:`main` passing the CLI arguments extracted from :obj:`sys.argv`

    This function can be used as entry point to create console scripts with setuptools.
    """
    main(sys.argv[1:])


if __name__ == "__main__":
    # ^  This is a guard statement that will prevent the following code from
    #    being executed in the case someone imports this file instead of
    #    executing it as a script.
    #    https://docs.python.org/3/library/__main__.html

    # After installing your project with pip, users can also run your Python
    # modules as scripts via the ``-m`` flag, as defined in PEP 338::
    #
    #     python -m testapi.testapi
    #
    run()
